## Synopsis
A NodeJS app to provide:
1. API to search Github users star-count  
2. A browser interface to interact with the API and print results

## install nodejs, node package manager, redis-server, download this git, then install this app
```
apt-get install -y nodejs npm redis-server
git clone https://shallawell@gitlab.com/shallawell/BlueWebGetGitStars.git
redis-server & 
cd BlueWebGetGitStars 
npm install express redis axios response-time body-parser --save
nodejs app.js
```
## Motivation
To further my personal software development skills and usage of tech such as Redis, Nodejs, Express framework, API development, HTML, CSS,

## Usage 
| Browser                  | API                                   |
| -------------------------|---------------------------------------|
| `http://localhost:8000/` | `http://localhost:8000/api/status`    |
|                          | `http://localhost:8000/api/shallawell`|

## Author
@shallawell

## License
MIT

## Acknowledgements
(http://www.ibm.com/developerworks/cloud/library/cl-bluemix-node-redis-app/index.html) <br>
(https://coligo.io/nodejs-api-redis-cache/)